package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
