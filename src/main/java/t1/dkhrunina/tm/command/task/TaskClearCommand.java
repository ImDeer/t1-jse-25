package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[Clear task list]");
        getTaskService().clear(getUserId());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}